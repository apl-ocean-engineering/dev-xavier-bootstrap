# dev-xavier-boostrap

Instructions and scripts for codifying the setup of our Xavier AGX development board (or really, any Jetson device) as a generic development platofmr.

The current instructions are for **Jetpack 5.1.2**.

The setup process spans three major tasks:

1. Preliminary setup of a board using NVidia SDK Manager.  This installs the preliminary kernel and root filesystem.   It also sets up the initial user and installs core NVidia libraries (CUDA, VPI, etc).
2. Ansible scripts from [this repo](ansible/) are run to further configure the environment.

**Note::** These scripts are for setting up system-level services and installing packages.  They don't create secondary users, as that may change on a install-by-install basis.

There steps are detailed below:

# 1. Preliminary set up with SDK Manager

For now we use SDK manager for initial setup of the board .  Future version may flash a customized rootfs or use device cloning.

Running the latest [SDK Manager](https://developer.nvidia.com/sdk-manager), select "Jetpack 5.1.2."

Press the recovery button while (re)booting the board.  Install the USB-C serial cable to the **front** of the unit (not the USB-C connector near the power plug).  SDK Manager should recognize the board:

![](images/sdk_manager.png)

When prompted, select "Xavier AGX" as the unit is already in recovery mode.   Then provide the initial user "sysop" with the default password:

![](images/sdk_manager_connection.png)

And select "eMMC".

SDK Manager will now flash the unit and install the root filesystem.

Eventually it will prompt for login info to install additional SDK components:

![](images/sdk_manager_install_sdk_components.png)

I found the USB network connection was not bulletproof and connecting over ethernet was more reliable.

When complete it, SDK manager will provide a summary then quit.

You can then SSH into the unit, or power it down and remove the recovery jumper.

# 1a.  Configure onboard NVMe as home directory

SSH into the board.

The following instructions configure the NVMe drive as `/home` with the BTRFS filesystem. Other filesystem types can be used as required.

(these instructions are just a brief sketch)

1. Create a partition on the NVMe drive
2. Initialize the partition as BTRFS
3. Get the blkid:

```
blkid /dev/nvme1n1p1
```

4. Mount the drive at `/mnt/temp`
5. Copy `/home/* to /mnt/temp`
6. Add to `fstab`:

```
UUID="xxx-xxxxx-xxxx-xxxx"  /home	btrfs	defaults	0	0
```

7. Mount:

```
fstab -a
```

# 2. Run Ansible scripts

We use `ansible-pull` to **PULL** ansible configuration from this Gitlab repo to the local machine.  For repeatability, the bootstrap script looks for a configuration file `~/.xavier_bootstrap.sh` which contains:

```
export XAVIER_HOSTNAME=<hostname>
```

Create the file then:

```
wget -O- https://gitlab.com/apl-ocean-engineering/dev-xavier-bootstrap/-/raw/main/ansible_bootstrap.sh | /usr/bin/bash
```




## Running ansible manually

To run the ansible manually, checkout out the git repo and use `ansible-playbook`

```
source ~/.xavier_bootstraps.sh
git clone -b orin_nx https://gitlab.com/rsa-perception-sensor/xavier_bootstrap.git
cd xavier_bootstrap
ansible-playbook -vvv --extra-vars "var_hostname=$XAVIER_HOSTNAME" ansible/xavier.yml
```
