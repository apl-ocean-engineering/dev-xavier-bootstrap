#!/usr/bin/bash

set -e

if [[ -f $HOME/.xavier_bootstrap.sh ]]; then
    echo "Using configuration from ~/.xavier_bootstrap.sh"
    source ~/.xavier_bootstrap.sh
else
    echo "Please set configuration in ~/.xavier_bootstrap.sh:"
    echo ""
    echo "   export XAVIER_HOSTNAME=<hostname>"
    exit -1
fi

echo "Using this configuration:"
echo "   XAVIER_HOSTNAME=$XAVIER_HOSTNAME"
echo ""

sudo apt install -y --no-install-recommends software-properties-common git
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt install -y --no-install-recommends ansible

sudo ansible-pull -vvv \
            --extra-vars "var_hostname=$XAVIER_HOSTNAME" \
            -C main \
            -U https://gitlab.com/apl-ocean-engineering/dev-xavier-bootstrap \
            ansible/xavier.yml
